Compress Jquery : 

git clone https://github.com/jquery/jquery.git
git checkout 3.7.1
npm install
Place jquery at end of body tag, since we exclude core/ready also (or call jquery functions within window.onload)
4.x -> npm run build -- -e wrap -e exports/amd -e dimensions -e ajax/jsonp -e ajax/script -e deprecated -e selector -e core/ready

3.7.1 -> npx grunt custom:-ajax/jsonp,-ajax/script,-deprecated,-dimensions,-wrap,-selector,-exports/amd,-core/ready

Compress Images: 
cwebp -q 100 -m 6 -lossless -o logo.webp logo.jpg


