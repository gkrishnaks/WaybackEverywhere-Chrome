import globals from "globals";
import pluginJs from "@eslint/js";

/** @type {import('eslint').Linter.Config[]} */
export default [
  {
    languageOptions: {
      globals: {
        ...globals.webextensions,
        ...globals.browser,
        $: true,
      },
    },
    rules: {
      "no-undef": "error",
      "prefer-const": "warn",
    },
  },
  pluginJs.configs.recommended,
];
