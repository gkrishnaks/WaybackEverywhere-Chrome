/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

import { log } from "../devTools/log.js";
import { UrlHelper } from "../commonUtils/parseUrl.js";
import { state } from "../workers/state.js";

function tabsUpdates(url, activetab, tabid) {
  chrome.tabs.update(tabid, {
    active: activetab,
    url,
  });
}

function checkTempExcludes(domain, tempExcludes) {
  // Check and add TempExcludes if Category is AddtoTempExcludesList
  log(`Temp excludes before..${tempExcludes}`);
  let temp = [];
  const tempExc = tempExcludes;
  if (tempExc !== null && tempExc !== undefined) {
    temp = tempExcludes.map((item) => {
      let data = item.replaceAll("*", "");
      data = data.replaceAll("|", "");
      return data;
    });
    if (temp.indexOf(domain) > -1) {
      log(
        `${
          domain
        } already exists in tempexcludes, just calling addsitetoexclude without saving`,
      );
      return;
    }
    tempExc.push(`|*${domain}*`);
    log(
      `${
        domain
      } does not exist in tempexcludes, saving to storage tempExcludes${
        tempExc
      }`,
    );

    chrome.storage.local.set({
      tempExcludes: tempExc,
    });
  }
}

function addSitetoExclude(request, sender, sendResponse) {
  const redirectslist = [];
  log(`addSitetoExclude request is ${JSON.stringify(request)}`);
  chrome.storage.local.get(
    {
      redirects: [],
      tempExcludes: [],
      tempIncludes: [],
    },
    (response) => {
      const { tempIncludes } = response;
      const { tempExcludes } = response;
      for (let i = 0; i < response.redirects.length; i += 1) {
        redirectslist.push(response.redirects[i]);
      }
      let url1 = "";

      let tabid = "";
      let activetab = false;

      if (request.subtype === "fromPopup") {
        tabid = request.tabid;
        url1 = request.url;
        activetab = true;
      } else {
        tabid = sender.tab.id;
        url1 = sender.tab.url;
      }

      log(`tabid is..${tabid}`);
      const obj = UrlHelper.getHostfromUrl(url1);
      log(
        `${obj.hostname} and outputurl ${
          obj.url
        } received from parseUrl.js for input Url ${url1}`,
      );

      // check if already exists in ExcludePattern
      const array = redirectslist[0].excludePattern.split("*|*");
      const lastIndex = array.length - 1;
      array[lastIndex] = array[lastIndex].replace("*", "");

      let alreadyExistsinExcludes = false;
      if (array.indexOf(obj.hostname) > -1) {
        alreadyExistsinExcludes = true;
      }

      // Fix for https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/13
      // t.co seems to be the only hostname that causes problems with other sites
      // that have "somenamet.com" in url where "t.co" gets a match against t.co

      if (
        !alreadyExistsinExcludes &&
        obj.hostname.length > 0 &&
        obj.hostname !== "t.co" &&
        obj.hostname !== "web.archive.org"
      ) {
        state.onBeforeNavigateCountsInWorkerInstance = -1; // skip next request for average calculation as partitionedRedirects will be rebuilt in background.js
        log(`need to exclude this site : ${obj.hostname}`);
        redirectslist[0].excludePattern = `${redirectslist[0].excludePattern}|*${obj.hostname}*`;
        log(
          `Now the new redirects is${JSON.stringify(redirectslist, null, 2)}`,
        );

        chrome.storage.local.set(
          {
            redirects: redirectslist,
          },
          // eslint-disable-next-line no-unused-vars
          (_a) => {
            log("Finished saving redirects to storage from url");
            log(`Need to reload page with excluded url.. ${obj.url}`);
            tabsUpdates(obj.url, activetab, tabid);
          },
        );
      } else {
        log(
          "domainname already exists in excludes list, just loading live page",
        );
        tabsUpdates(obj.url, activetab, tabid);
      }
      // Check if it's a temporary exclude request and put in temp exclude list too
      if (request.category === "AddtoTempExcludesList") {
        checkTempExcludes(obj.hostname, tempExcludes);
      } else {
        // request.category is 'AddtoExcludesList'
        // remove this domain from tempIncludes if it exists since
        // since wayback machine cannot archive these sites
        const index = tempIncludes.indexOf(`|*${obj.hostname}*`);
        if (index > -1 && request.subtype !== "fromPopup") {
          tempIncludes.splice(index, 1);
          chrome.storage.local.set({ tempIncludes });
        }
      }
      sendResponse({
        message: "site  excluded",
      });
    },
  );
}

// eslint-disable-next-line no-extend-native, func-names
String.prototype.replaceAll = function (searchStr, replaceStr) {
  const str = this;

  // escape regexp special characters in search string
  // eslint-disable-next-line no-param-reassign, no-useless-escape
  searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");

  return str.replace(new RegExp(searchStr, "gi"), replaceStr);
};

export { addSitetoExclude };
