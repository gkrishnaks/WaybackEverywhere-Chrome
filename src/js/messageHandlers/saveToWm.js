/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

import { log } from "../devTools/log.js";
import { UrlHelper } from "../commonUtils/parseUrl.js";

async function savetoWM(request, sender, sendResponse) {
  let url1 = "";
  let tabid;
  let activetab = true;
  if (request.subtype === "fromContent") {
    url1 = sender.tab.url;
    tabid = sender.tab.id;
    activetab = false;
  }
  if (request.subtype === "fromPopup") {
    tabid = request.tabid;
    url1 = request.url;
  }
  let toSave;
  if (url1.indexOf("web.archive.org") > -1) {
    const obj = UrlHelper.getHostfromUrl(url1);
    toSave = obj.url.replace("#close", "");
  } else {
    toSave = url1.replace("#close", "");
  }
  const results = await chrome.storage.local.get({
    filters: [],
    counts: {},
  });
  const { justSaved } = await chrome.storage.session.get({ justSaved: {} });
  const { filters, counts } = results;
  toSave = UrlHelper.cleanUrlsOnFilters(toSave, filters);

  if (request.subtype === "fromContent") {
    const isJustSaved = justSaved[toSave];
    if (isJustSaved) {
      const message = `this page is in justSaved, so skipping save action for ${toSave}`;
      log(message);
      sendResponse({
        message,
      });
      return;
    }
  }

  justSaved[toSave] = Date.now();
  chrome.storage.session.set({ justSaved });
  const wmSaveUrl = `https://web.archive.org/save/${toSave}`;
  log(`save url to be loaded is -- also cleaned with filters --${wmSaveUrl}`);
  // headerHandler - Make this as browser's UserAgent for "Save" requests - "WaybackEverywhere"
  // Wayback Machine Team requested for an unique useragent so that they can audit "save page"
  // requests that are sent from this extension/addon - https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/4
  try {
    await chrome.declarativeNetRequest.updateSessionRules({
      addRules: [
        {
          id: 1,
          priority: 1,
          action: {
            type: "modifyHeaders",
            requestHeaders: [
              {
                header: "user-agent",
                operation: "set",
                value:
                  "Save Page Request from WaybackEverywhere Browser Extension",
              },
              {
                header: "cookie",
                operation: "set",
                value: "",
              },
            ],
          },
          condition: {
            urlFilter: "https://web.archive.org/save/*",
            resourceTypes: ["main_frame"],
          },
        },
      ],
    });
  } catch (E) {
    /* empty */
  }

  chrome.tabs.update(
    tabid,
    {
      active: activetab,
      url: wmSaveUrl,
    },
    async (tab) => {
      //      counts.waybackSavescount

      log(
        `in success function of tab update ${tab.url} save count:${counts.waybackSavescount}`,
      );
      // counts update for /save happens from background.js since mv3 uses short lived worker and that causes issue with reading back before next page load
      // counts.waybackSavescount += 1;
      // await chrome.storage.local.set({
      //   counts,
      // });
      //  log(`savecount:${counts.waybackSavescount}`);
      sendResponse({
        message: "saving page",
      });
      try {
        await chrome.declarativeNetRequest.updateSessionRules({
          removeRuleIds: [1],
        });
      } catch (e) {
        /* empty */
      }
    },
  );
}

export { savetoWM };
