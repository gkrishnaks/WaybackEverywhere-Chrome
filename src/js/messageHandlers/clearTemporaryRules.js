/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

import { log } from '../devTools/log.js';

function clearTemps(domain, toClear, sendResponse) {
  chrome.storage.local.get({
    tempExcludes: [],
    tempIncludes: [],
  }, ({ tempIncludes, tempExcludes }) => {
    let index = -1;
    if (toClear === 'cleartempIncludes') {
      index = tempIncludes.indexOf(`|*${domain}*`);
      if (index > -1) {
        tempIncludes.splice(index, 1);
      }

      chrome.storage.local.set({ tempIncludes }, () => {
        sendResponse({
          message: 'tempRulesCleared',
        });
      });
    } else {
      index = tempExcludes.indexOf(`|*${domain}*`);
      if (index > -1) {
        tempExcludes.splice(index, 1);
      }

      chrome.storage.local.set({ tempExcludes }, () => {
        sendResponse({
          message: 'tempRulesCleared',
        });
      });
    }
  });
}

// eslint-disable-next-line no-extend-native, func-names
String.prototype.replaceAll = function (searchStr, replaceStr) {
  const str = this;

  // escape regexp special characters in search string
  // eslint-disable-next-line no-param-reassign, no-useless-escape
  searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');

  return str.replace(new RegExp(searchStr, 'gi'), replaceStr);
};

function clearAllTemps(sendResponse) {
  // remove "temporarily exclude sites" on startup
  // Or user opeted to clear Temporary settings from Settings page
  let isChanged = false;
  chrome.storage.local.get(
    {
      tempExcludes: [],
      tempIncludes: [],
      redirects: [],
    },
    (obj) => {
      const { redirects } = obj;
      const excarray = obj.tempExcludes;
      log(`exclude array on startup is...${excarray}`);
      if (excarray.length > 0) {
        isChanged = true;
        for (let i = 0; i < excarray.length; i += 1) {
          const toReplace = excarray[i];
          log(`${toReplace} need to be removed from exclude pattern`);
          redirects[0].excludePattern = redirects[0].excludePattern.replaceAll(
            toReplace,
            '',
          );

          log(`Redirects from storage: ${redirects[0].description}`);
        }
      }

      const incarray = obj.tempIncludes;
      log(`include array on startup to reset...${incarray}`);
      if (incarray.length > 0) {
        for (let i = 0; i < incarray.length; i += 1) {
          const toAdd = incarray[i];
          redirects[0].excludePattern += toAdd;
        }
        isChanged = true;
      }
      if (isChanged) {
        const temp = [];
        chrome.storage.local.set(
          {
            redirects,
            tempExcludes: temp,
            tempIncludes: temp,
          },
          () => {
            sendResponse({
              message: 'successfullyclearedTemps',
            });
          },
        );
      }
    },
  );
}

export { clearTemps, clearAllTemps };

/* TODO
                 else if( domain.indexOf(".") !== domain.lastIndexOf(".")){
                  log("in subdomain check...");
                  let i = domain.indexOf(".");
                  index= tempIncludes.indexOf('|*' + domain.substring(i+1) + '*');
                  if(index > -1){
                  tempIncludes.splice(index,1);
                }
                } */
/* TODO
else if( domain.indexOf(".") !== domain.lastIndexOf(".")){
        log("in subdomain check...");

  let i = domain.indexOf(".");
  index= tempExcludes.indexOf('|*' + domain.substring(i+1) + '*');
  if(index > -1){
  tempExcludes.splice(index,1);
}
} */
