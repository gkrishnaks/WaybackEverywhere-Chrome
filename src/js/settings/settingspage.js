/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

/* eslint-disable func-names */

// This is the main controller of the page. It is responsible for showing messages,
// modal windows and loading and saving the list of redirects, that all of the
// controllers work with.
import { Redirect } from "../commonUtils/redirect.js";
import { UrlHelper } from "../commonUtils/parseUrl.js";

const App = {};
App.settingsApp = {};
App.settingsApp.newExcludesite = "";
App.settingsApp.newIncludeSite = "";

App.settingsApp.operationmode = false;
// we consider false as Default ON, true as disable on browser startup..

App.settingsApp.storage = chrome.storage.local;
// TODO: Change to sync when Firefox supports it...
App.settingsApp.showCopyButtons = true;

App.settingsApp.getInitialValues = () => {
  App.settingsApp.storage.get(
    {
      operationMode: false,
      logging: false,
      showCopyButtons: true,
    },
    (obj) => {
      App.settingsApp.getRules();
      App.settingsApp.logging = obj.logging;
      const openAdvanced = document.getElementById("openAdvanced");
      if (openAdvanced) {
        openAdvanced.hidden = !obj.logging;
      }

      document.getElementById("toggleLogging").checked = obj.logging;
      // const useragent = navigator.userAgent;
      App.settingsApp.operationmode = obj.operationMode;
      document.getElementById("toggleOperationMode").checked =
        obj.operationMode;
      document.getElementById("toggleShowCopyButton").checked =
        obj.showCopyButtons;
      App.settingsApp.showCopyButtons = obj.showCopyButtons;
    },
  );
};

App.settingsApp.addtoExclude = (url) => {
  App.settingsApp.message = null;
  // eslint-disable-next-line no-useless-escape
  const url1 = url.replace(/[\|&;\$%@"<>\(\)\+\^\'\*,]/g, "");
  const exclPatrn = App.settingsApp.getPattern(url1);
  let msg;
  App.settingsApp.newExcludesite = "";
  if (url1.length < 4) {
    msg = `You entered a URL -> ${url1} which is too small and will affect addon functionality. Please try again. Reach out to Developer if this is causing an issue`;
    App.settingsApp.showMessage(msg, false, 10, 1);

    return;
  }

  if (UrlHelper.hasRepeatedLetters(url1)) {
    App.settingsApp.showMessage(
      "Invalid URL, try again or reach out to developer if this is causing an issue",
      false,
    );
    return;
  }
  if (exclPatrn.domain.indexOf("|") > -1) {
    msg = `You entered a URL -> ${exclPatrn.domain} with | symbol. This is not allowed, please try again`;
    App.settingsApp.showMessage(msg, false, 10, 1);
    App.settingsApp.newExcludesite = "";

    return;
  }
  // excludePattern in App.settingsApp.redir looks like *web.archive.org*|*fsf.org*
  const str = App.settingsApp.redirects[0].excludePattern;
  const array = str.split("*");
  if (array.indexOf(exclPatrn.domain) > -1) {
    msg = `This site ${exclPatrn.domain} already exists in Excludes list`;
    App.settingsApp.newExcludesite = "";

    App.settingsApp.showMessage(msg, false, 10, 1);
  } else {
    App.settingsApp.redirects[0].excludePattern += exclPatrn.Pattern;
    msg = `This site ${exclPatrn.domain} is added to Excludes list`;
    App.settingsApp.newExcludesite = "";

    const arr = App.settingsApp.redirects.map(App.settingsApp.normalize);
    App.settingsApp.saveRedirectsMsgsender(arr);
    App.settingsApp.getRules();
    App.settingsApp.showMessage(msg, "success", 10, 1);
  }
};

App.settingsApp.tempExcludes = "";
App.settingsApp.tempIncludes = "";
App.settingsApp.justSaved = "";
App.settingsApp.filters = [];
App.settingsApp.appVersion = "1.0.0";
App.settingsApp.commonExtensions = [];
App.settingsApp.getTemps = () => {
  chrome.runtime.sendMessage(
    {
      type: "appDetails",
      subtype: "fromSettings",
      domain: "settings.html",
    },
    (response) => {
      /* log.enabled = response.logstatus;
        let counts = JSON.parse(response.counts);
        // {"archivedPageLoadsCount":0,"waybackSavescount":0}
        App.settingsApp.savecount = counts.waybackSavescount;
        App.settingsApp.loadcount = counts.archivedPageLoadsCount;
        App.settingsApp.disabled = response.appDisabled; */
      App.settingsApp.appVersion = response.appVersion;
      const k = document.getElementById("appVersion");
      if (k) {
        k.textContent = `Version ${App.settingsApp.appVersion}`;
      }
      App.settingsApp.tempExcludes = response.tempExcludes
        .join("")
        .replace(/\*/g, "")
        .substring(1)
        .replace(/\|/g, ", ");
      let el = document.getElementById("tempExcludes");

      if (App.settingsApp.tempExcludes) {
        el.style.display = "";
        el.lastElementChild.textContent = App.settingsApp.tempExcludes;
      } else {
        el.style.display = "none";
      }
      el = document.getElementById("tempIncludes");
      App.settingsApp.tempIncludes = response.tempIncludes
        .join("")
        .replace(/\*/g, "")
        .substring(1)
        .replace(/\|/g, ", ");
      if (App.settingsApp.tempIncludes) {
        el.style.display = "";
        el.lastElementChild.textContent = App.settingsApp.tempIncludes;
      } else {
        el.style.display = "none";
      }

      if (!App.settingsApp.tempExcludes && !App.settingsApp.tempExcludes) {
        document.getElementById("clearTempButton").style.display = "none";
      } else {
        document.getElementById("clearTempButton").style.display = "";
      }
      App.settingsApp.isLoadAllLinksEnabled = response.isLoadAllLinksEnabled;
      //  console.log('tempExcludes is ' + tempExcludes + ' tempIncludes is ' + tempIncludes);
      const justSavedList = Object.keys(response.justSaved);
      if (justSavedList.length > 0) {
        App.settingsApp.justSaved = justSavedList.join(", ");
      }
      App.settingsApp.filters = response.filters;
      App.settingsApp.appVersion = response.appVersion;
      App.settingsApp.commonExtensions = response.commonExtensions;
      App.settingsApp.showdebuginfo();
      // App.settingsApp.getRules();;
    },
  );
};

App.settingsApp.clearTemps = () => {
  chrome.runtime.sendMessage(
    {
      type: "clearTemps",
    },
    (response) => {
      if (response.message === "successfullyclearedTemps") {
        App.settingsApp.tempExcludes = "";
        App.settingsApp.tempIncludes = "";
        // App.settingsApp.getRules();;
        App.settingsApp.getRules();
      }
    },
  );
};

App.settingsApp.removefromExclude = (url) => {
  // eslint-disable-next-line no-useless-escape
  const url1 = url.replace(/[\|&;\$%@"<>\(\)\+\^\'\*,]/g, "");
  App.settingsApp.message = null;
  const inclPattrn = App.settingsApp.getPattern(url1);
  let msg;
  App.settingsApp.newIncludeSite = "";
  if (inclPattrn.domain.indexOf("|") > -1) {
    msg = `You entered a URL -> ${inclPattrn.domain} with | symbol. This is not allowed, please try again`;
    App.settingsApp.showMessage(msg, false, 10, 1);
    App.settingsApp.newIncludeSite = "";
    App.settingsApp.getRules();

    return;
  }
  if (inclPattrn.domain.indexOf("web.archive.org") > -1) {
    msg = `You entered a URL -> ${inclPattrn.domain} This is not allowed, removing this will affect functionality`;
    App.settingsApp.showMessage(msg, false, 10, 1);
    App.settingsApp.newIncludeSite = "";
    App.settingsApp.getRules();

    return;
  }
  const str = App.settingsApp.redirects[0].excludePattern;
  const array = str.split("*");
  // Using >0 because we shouldn't let remove web.archive.org from excludeslist -
  // avoiding endless redirect loop
  if (array.indexOf(inclPattrn.domain) > -1) {
    App.settingsApp.newIncludeSite = "";

    const obj = App.settingsApp.replaceAll(
      App.settingsApp.redirects[0].excludePattern,
      inclPattrn.domain,
    );
    if (obj.isremoved) {
      App.settingsApp.redirects[0].excludePattern = obj.string;
      msg = `This site ${inclPattrn.domain} is removed from the Excludes list`;
      const arr = App.settingsApp.redirects.map(App.settingsApp.normalize);
      App.settingsApp.saveRedirectsMsgsender(arr);
      App.settingsApp.showMessage(msg, true, 10, 1);
    } else {
      msg = `No need to remove.. This site ${inclPattrn.domain} is already not available in the Excludes list`;
      App.settingsApp.showMessage(msg, "success", 10, 1);
    }
  } else {
    App.settingsApp.newIncludeSite = "";
    msg = `No need to remove! This site ${inclPattrn.domain} is already not available in the Excludes list`;
    App.settingsApp.showMessage(msg, "success", 10, 1);
  }
  App.settingsApp.getRules();
};

App.settingsApp.isLoadAllLinksEnabled = false;

App.settingsApp.storage.get(
  {
    isLoadAllLinksEnabled: false,
  },
  (obj) => {
    App.settingsApp.isLoadAllLinksEnabled = obj.isLoadAllLinksEnabled;
    document.getElementById("isLoadAllLinksEnabled").checked =
      obj.isLoadAllLinksEnabled;
    // App.settingsApp.getRules();;
  },
);

App.settingsApp.toggleLoadAllLinksSettings = function () {
  App.settingsApp.storage.set(
    {
      isLoadAllLinksEnabled: !App.settingsApp.isLoadAllLinksEnabled,
    },
    () => {
      App.settingsApp.isLoadAllLinksEnabled =
        !App.settingsApp.isLoadAllLinksEnabled;

      if (App.settingsApp.isLoadAllLinksEnabled) {
        document.getElementById("showExampleOpen").style.display = "";
        document.getElementById("showExampleOpenall").style.display = "none";
      }

      // App.settingsApp.getRules();;
    },
  );
};

App.settingsApp.replaceAll = (exclist, site) => {
  let domain = site;
  // format *web.archive.org*|*example.org*
  const obj = {
    string: "",
    isremoved: false,
  };
  const array = exclist.split("|");
  domain = `*${domain}*`;

  for (let i = array.length - 1; i >= 0; i -= 1) {
    if (array[i] === domain) {
      array.splice(i, 1);
      obj.isremoved = true;
    }
  }
  obj.string = array.join("|");
  return obj;
};

App.settingsApp.getPattern = (url) => {
  let url2 = url;
  const obj = {
    domain: "",
    Pattern: "",
  };
  let url3;
  if (url2.indexOf("web.archive.org") >= 0) {
    if (url2.indexOf("web.archive.org/save") > -1) {
      url2 = url2.replace(".org/save/", ".org/web/2/");
      // log('url2 from save to web/2 ..' + url2);
    }
    url2 = url2.split("web.archive.org/").pop();
  }
  if (url2.indexOf("http://") < 0 && url2.indexOf("https://") < 0) {
    url3 = url2;
    // log('url3 set as ' + url3);
    obj.domain = url2.toLowerCase();
  } else {
    const pattern = /:\/\/(.[^/]+)/;
    // eslint-disable-next-line prefer-destructuring
    url3 = url2.match(pattern)[1];
    url3 = url3.split("www.").pop();
    obj.domain = url3.toLowerCase();
    // log('excludethisSite after regexpattern is ' + url3);
  }
  obj.Pattern = `|*${url3.toLowerCase()}*`;
  return obj;
};

App.settingsApp.openHelp = (page) => {
  const url = chrome.runtime.getURL(page);

  // FIREFOXBUG: Firefox chokes on url:url filter if the url is a moz-extension:// url
  // so we don't use that, do it the more manual way instead.
  chrome.tabs.query(
    {
      currentWindow: true,
    },
    (tabs) => {
      for (let i = 0; i < tabs.length; i += 1) {
        if (tabs[i].url === url) {
          chrome.tabs.update(
            tabs[i].id,
            {
              active: true,
            },
            // eslint-disable-next-line no-unused-vars
            (_tab) => {
              // close();
            },
          );
          return;
        }
      }

      chrome.tabs.create({
        url: page,
      });
    },
  );
};

// Cleans out any extra props, and adds default values for missing ones.
App.settingsApp.normalize = (r) => new Redirect(r).toObject();

App.settingsApp.showresetConfirmation = false;

App.settingsApp.saveRedirectsMsgsender = (arr) => {
  chrome.runtime.sendMessage(
    {
      type: "saveredirects",
      redirects: arr,
    },
    // eslint-disable-next-line no-unused-vars
    (_response) => {
      App.settingsApp.redirects = [];
      App.settingsApp.redirects = arr;
      App.settingsApp.ReadableExcludePattern =
        App.settingsApp.redirects[0].excludePattern
          .replace(/\*/g, "")
          .replace(/\|/g, ", ");
      App.settingsApp.getRules();
      // App.settingsApp.getRules();;
    },
  );
};

// Saves the entire list of redirects to App.settingsApp.storage
App.settingsApp.saveChanges = () => {
  // Clean them up so angular $$hash things and stuff don't get serialized.
  const arr = App.settingsApp.redirects.map(App.settingsApp.normalize);

  // adding web.archive.org to beginning of exclude so that even if user clears it, ,
  // so that user won't end up endless looping
  if (arr[0].excludePattern.indexOf("web.archive.org") < 0) {
    if (arr[0].excludePattern.length === 0) {
      arr[0].excludePattern =
        "*web.archive.org*|*archive.org*|*chrome://*|*about:*|*/chrome/newtab*";
    } else {
      arr[0].excludePattern = `*web.archive.org*|*archive.org*|*chrome://*|*about:*|*/chrome/newtab*|${arr[0].excludePattern}`;
    }
  }

  App.settingsApp.saveRedirectsMsgsender(arr);
};

// Saves the entire list of redirects to App.settingsApp.storage
App.settingsApp.saveChanges2 = (arr) => {
  // Clean them up so angular $$hash things and stuff don't get serialized.

  // adding web.archive.org to beginning of exclude so that even if imported json doesn't have it,
  // so that user won't end up endless looping

  if (arr[0].excludePattern.indexOf("web.archive.org") < 0) {
    // eslint-disable-next-line no-param-reassign
    arr[0].excludePattern = `*web.archive.org*|*archive.org*|*chrome://*|*about:*|${arr[0].excludePattern}`;
  }
  App.settingsApp.saveRedirectsMsgsender(arr);
};

App.settingsApp.redirects = [];

App.settingsApp.ReadableExcludePattern = "web.archive.org";
// Need to proxy this through the background page,because Firefox gives us dead objects
// nonsense when accessing chrome.Storage directly.
App.settingsApp.getRules = () => {
  chrome.runtime.sendMessage(
    {
      type: "getredirects",
    },
    (response) => {
      if (App.settingsApp.redirects.length === 0) {
        App.settingsApp.redirects.push(
          App.settingsApp.normalize(response.redirects[0]),
        );
      }
      document.getElementById("rDescription").textContent =
        App.settingsApp.redirects[0].description;
      App.settingsApp.ReadableExcludePattern =
        response.redirects[0].excludePattern
          .replace(/\*/g, "")
          .replace(/\|/g, ", ");
      document.querySelector("#showExcludes").textContent =
        App.settingsApp.ReadableExcludePattern;
      App.settingsApp.getTemps();
      App.settingsApp.showdebuginfo();

      // App.settingsApp.getRules();;
    },
  );
};

// Shows a message explaining how many redirects were imported.

App.settingsApp.timestamp = new Date();

// eslint-disable-next-line func-names
App.settingsApp.importRedirects = function () {
  const fileInput = document.getElementById("fileInput");
  // eslint-disable-next-line no-unused-vars
  fileInput.addEventListener("change", (_e) => {
    const file = fileInput.files[0];

    const reader = new FileReader();

    // eslint-disable-next-line no-unused-vars
    reader.onload = function (_e2) {
      let data;
      try {
        data = JSON.parse(reader.result);
        // eslint-disable-next-line no-console
        console.log(`Wayback Everywhere: Imported data${JSON.stringify(data)}`);
      } catch (e) {
        App.settingsApp.showMessage(
          `Failed to parse JSON data, invalid JSON: ${(e.message || "").substr(
            0,
            100,
          )}`,
        );
        return;
      }

      if (!data.redirects) {
        App.settingsApp.showMessage(
          'Invalid JSON, missing "redirects" property ',
        );
        return;
      }

      if (
        data.createdBy.indexOf("Wayback Everywhere") < 0 ||
        data.redirects[0].description.indexOf("Wayback Everywhere") < 0
      ) {
        App.settingsApp.showMessage(
          "Invalid JSON, this does not seem to be exported from Wayback Everywhere",
        );
        return;
      }

      let imported = 0;
      let existing = 0;

      const i = 0;
      const r = new Redirect(data.redirects[i]);
      //r.updateExampleResult();
      if (
        App.settingsApp.redirects.some((item) => new Redirect(item).equals(r))
      ) {
        existing += 1;
      } else {
        const arr = [];
        arr.push(r.toObject());

        imported += 1;
        App.settingsApp.saveChanges2(arr);
      }

      App.settingsApp.showImportedMessage(imported, existing);

      // add above this
    };

    try {
      reader.readAsText(file, "utf-8");
    } catch (e) {
      App.settingsApp.showMessage("Failed to read import file");
    }
  });
};

// Updates the export link with a data url containing all the redirects.
// We want to have the href updated instead of just generated on click to
// allow people to right click and choose Save As...
App.settingsApp.updateExportLink = () => {
  const d = new Date();
  let localtime = d.toTimeString().split("GMT").shift("GMT");
  localtime = localtime.trim();
  let localtimezone = d.toTimeString().split("(").pop("GMT").replace(")", "");
  localtimezone = localtimezone.trim();
  let day = d.toDateString();
  day = day.substring(4).trim().replace(/ /g, ".");
  App.settingsApp.timestamp = `${day}_${localtime}_${localtimezone}`;

  const redirects = App.settingsApp.redirects.map((r) =>
    new Redirect(r).toObject(),
  );

  const exportObj = {
    createdBy: `Wayback Everywhere v${chrome.runtime.getManifest().version}`,
    createdAt: new Date(),
    redirects, // TODO Add filters, fileExtensions to export/import
  };

  const json = JSON.stringify(exportObj, null, 4);

  // Using encodeURIComponent here instead of base64
  // because base64 always messed up our encoding for some reason...
  App.settingsApp.redirectDownload = `data:text/plain;charset=utf-8,${encodeURIComponent(
    json,
  )}`;
  const elem = document.getElementById("exportSettingBtn");
  if (elem) {
    elem.href = App.settingsApp.redirectDownload;
    elem.download = elem.download.replace(
      "{{timestamp}}",
      App.settingsApp.timestamp,
    );
  }
};

App.settingsApp.showImportedMessage = (imported, existing) => {
  if (imported === 0 && existing === 0) {
    App.settingsApp.showMessage("No redirects existed in the file.");
  }
  if (imported > 0 && existing === 0) {
    App.settingsApp.showMessage("Successfully imported settings", true);
  }
  if (imported === 0 && existing > 0) {
    App.settingsApp.showMessage(
      "All redirects in the file already existed and were ignored.",
    );
  }
  if (imported > 0 && existing > 0) {
    let m = `Successfully imported ${imported} redirect${
      imported > 1 ? "s" : ""
    }. `;
    if (existing === 1) {
      m += "1 redirect already existed and was ignored.";
    } else {
      m += `${existing} redirects already existed and were ignored.`;
    }
    App.settingsApp.showMessage(m, true);
  }
};

App.settingsApp.showdebuginfo = () => {
  const debugItems = [].slice.call(
    document.getElementsByClassName("showWhenLogging"),
  );
  const el2 = document.getElementById("loggingInAdvancedUserPage");
  if (App.settingsApp.logging || !!el2) {
    debugItems.forEach((item) => {
      // eslint-disable-next-line no-param-reassign
      item.style.display = "";
    });

    document.getElementById("justsaved").textContent =
      App.settingsApp.justSaved;
    document.getElementById("filtersarea").textContent =
      App.settingsApp.filters.join(", ");
    document.getElementById("fileExtarea").textContent =
      App.settingsApp.commonExtensions.join(", ");
  } else if (debugItems) {
    debugItems.forEach((item) => {
      // eslint-disable-next-line no-param-reassign
      item.style.display = "none";
    });
  }
};

App.settingsApp.logging = false;

App.settingsApp.toggleLogging = () => {
  App.settingsApp.storage.get(
    {
      logging: false,
    },
    (obj) => {
      App.settingsApp.storage.set(
        {
          logging: !obj.logging,
        },
        () => {
          App.settingsApp.logging = !obj.logging;
          const openAdvanced = document.getElementById("openAdvanced");
          if (openAdvanced) {
            openAdvanced.hidden = !obj.logging;
          }

          App.settingsApp.showdebuginfo();
          App.settingsApp.getInitialValues();
        },
      );
    },
  );
};

App.settingsApp.toggleOperationMode = () => {
  App.settingsApp.storage.set(
    {
      operationMode: !App.settingsApp.operationmode,
    },
    // eslint-disable-next-line no-unused-vars
    (_a) => {
      App.settingsApp.operationmode = !App.settingsApp.operationmode;
      App.settingsApp.getRules();
    },
  );
};

App.settingsApp.doFactoryReset = () => {
  chrome.runtime.sendMessage({
    type: "doFullReset",
    subtype: "fromSettings",
  });
};

// Shows a message bar above the list of redirects.
App.settingsApp.showMessage = (message, success, timer, id) => {
  const msgBox = document.getElementById("message-box");
  const incExcmsg = document.getElementById("incExcmsg");
  if (id != null) {
    App.settingsApp.incExcmsg = message;
    incExcmsg.style.display = "";
    incExcmsg.innerText = message;
    incExcmsg.classList = success ? "success" : "error";
  } else {
    App.settingsApp.message = message;
    msgBox.style.display = "";
    msgBox.innerText = message;
    msgBox.classList = success ? "success" : "error";
  }
  if (timer == null) {
    // eslint-disable-next-line no-param-reassign
    timer = 10;
  }
  // Remove the message in 20 seconds if it hasnt been changed...

  setTimeout(() => {
    if (App.settingsApp.message === message) {
      App.settingsApp.message = null;
      msgBox.style.display = "none";
    }
  }, timer * 1000);
  setTimeout(() => {
    if (App.settingsApp.incExcmsg === message) {
      App.settingsApp.incExcmsg = null;
      incExcmsg.style.display = "none";
    }
  }, timer * 1000);
};

App.settingsApp.showresetConfirmation = false;
App.settingsApp.showresetbutton = true;
App.settingsApp.toggleResetwarn = () => {
  App.settingsApp.showresetConfirmation =
    !App.settingsApp.showresetConfirmation;
  App.settingsApp.showresetbutton = !App.settingsApp.showresetbutton;
  if (App.settingsApp.showresetConfirmation) {
    document.getElementById("showresetConfirmation").style.display = App
      .settingsApp.showresetConfirmation
      ? ""
      : "none";
  }
  App.settingsApp.showresetbutton = !App.settingsApp.showresetbutton;
  document.getElementById("toggleResetwarn").style.display = App.settingsApp
    .showresetbutton
    ? ""
    : "none";
};

App.settingsApp.toggleDisabled = (redirect) => {
  // eslint-disable-next-line no-param-reassign
  redirect.disabled = !redirect.disabled;
  App.settingsApp.saveChanges();
};

App.settingsApp.toggleShowCopyButton = () => {
  App.settingsApp.showCopyButtons = !App.settingsApp.showCopyButtons;
  chrome.storage.local.set({}, App.settingsApp.getInitialValues);
  App.settingsApp.storage.get(
    {
      showCopyButtons: true,
    },
    (obj) => {
      const showCopyButtons = !obj.showCopyButtons;

      chrome.storage.local.set(
        { showCopyButtons },
        App.settingsApp.getInitialValues,
      );
    },
  );
};

(() => {
  const hidden = {};
  if (typeof document.hidden !== "undefined") {
    // Opera 12.10 and Firefox 18 and later support
    hidden.visibilityChange = "visibilitychange";
  } else if (typeof document.msHidden !== "undefined") {
    hidden.visibilityChange = "msvisibilitychange";
  } else if (typeof document.webkitHidden !== "undefined") {
    hidden.visibilityChange = "webkitvisibilitychange";
  }

  document.addEventListener(
    hidden.visibilityChange,
    App.settingsApp.getInitialValues,
    false,
  );
})();

export { App };
