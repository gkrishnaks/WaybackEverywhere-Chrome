/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

/* eslint-disable func-names */

function accordionCollapseScroll() {
  // Get all elements with 'collapse' class
  const collapseElements = document.querySelectorAll('.collapse');
  
  collapseElements.forEach(element => {
    // Listen for Bootstrap collapse shown event
    element.addEventListener('shown.bs.collapse', (e) => {
      let card;
      if (e.target.className.includes('collapseCollect')) {
        card = e.target.closest('.collapseCollectButton');
      } else {
        card = e.target.closest('.accordion-item');
      }
      
      // Smooth scroll to the card
      const cardPosition = card.getBoundingClientRect().top + window.scrollY;
      window.scrollTo({
        top: cardPosition,
        behavior: 'smooth',
        duration: 500
      });
      
      e.stopPropagation();
    });
  });

}

let popupImageCollapses = [];
function collapseOpenImageOnClick(event) {
  if (event.target?.id) {
    const filteredCollapses = popupImageCollapses.filter((e) => e.id !== event.target.id);
    // eslint-disable-next-line no-restricted-syntax
    for (const filtered of filteredCollapses) {
      // eslint-disable-next-line no-undef
      bootstrap.Collapse?.getInstance(filtered)?.hide();
    }
  }
}

function updateVersionInFooter(){
  try {
    const manifestData = chrome.runtime.getManifest();
    const versionText = `Version ${manifestData.version}`;
    const appVersionNode = document.getElementById("appVersion")
    appVersionNode.innerText = versionText
  } catch (e) { /* empty */ }
}

window.onload = function () {
  fetch('helpDescription.html')
  .then(response => response.text())
  .then(html => {
    // innerHTML is safe here since it is just loading another html file bundled offline within the extension
    document.getElementById('helpDescriptionContent').innerHTML = html;
    accordionCollapseScroll();
    popupImageCollapses = [].slice.call(document.getElementsByClassName('collapseCollect'));
    if (popupImageCollapses.length > 0) {
      // eslint-disable-next-line no-restricted-syntax
      for (const popupImageCollapse of popupImageCollapses) {
        popupImageCollapse.addEventListener('show.bs.collapse', collapseOpenImageOnClick);
      }
    }
  updateVersionInFooter();
  }).catch(() =>{
    // console.log("error")
  });


};
