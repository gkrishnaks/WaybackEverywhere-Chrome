/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

function Redirect(o) {
  this._init(o);
}

// Static
Redirect.WILDCARD = "W";
Redirect.REGEX = "R";

Redirect.prototype = {
  // attributes
  description: "",
  exampleUrl: "",
  exampleResult: "",
  error: null,
  includePattern: "",
  excludePattern: "",
  redirectUrl: "",
  patternType: "",
  processMatches: "noProcessing",
  disabled: false,

  compile() {
    const incPattern = this._preparePattern(this.includePattern);
    const excPattern = this._preparePattern(this.excludePattern);
    // logs("Pattern.." + excPattern);
    if (incPattern) {
      this._rxInclude = new RegExp(incPattern, "i");
    }

    if (excPattern) {
      this._rxExclude = new RegExp(excPattern, "i");
    }
  },

  toObject() {
    return {
      description: this.description,
      exampleUrl: this.exampleUrl,
      exampleResult: this.exampleResult,
      error: this.error,
      includePattern: this.includePattern,
      excludePattern: this.excludePattern,
      redirectUrl: this.redirectUrl,
      patternType: this.patternType,
      processMatches: this.processMatches,
      disabled: this.disabled,
      appliesTo: this.appliesTo.slice(0),
    };
  },

  getMatch(url, hostname, forceIgnoreDisabled) {
    if (!this._rxInclude) {
      this.compile();
    }

    const result = {
      isMatch: false,
      isExcludeMatch: false,
      isDisabledMatch: false,
      redirectTo: "",
      toString() {
        return JSON.stringify(this);
      },
    };
    let redirectTo = null;

    redirectTo = this._includeMatch(url);
    // logs("redirectTo is" + this._includeMatch(url));

    if (redirectTo !== null) {
      if (this.disabled && !forceIgnoreDisabled) {
        result.isDisabledMatch = true;
      } else if (this._excludeMatch(hostname)) {
        result.isExcludeMatch = true;
      } else {
        result.isMatch = true;
        result.redirectTo = redirectTo;
      }
    }
    // logs("returning result as " + result);
    return result;
  },

  equals(redirect) {
    return (
      this.description == redirect.description &&
      this.exampleUrl == redirect.exampleUrl &&
      this.includePattern == redirect.includePattern &&
      this.excludePattern == redirect.excludePattern &&
      this.redirectUrl == redirect.redirectUrl &&
      this.patternType == redirect.patternType &&
      this.processMatches == redirect.processMatches &&
      this.appliesTo.toString() == redirect.appliesTo.toString()
    );
  },

  // Private functions below
  _rxInclude: null,
  _rxExclude: null,

  _preparePattern(pattern) {
    if (!pattern) {
      return null;
    }
    if (this.patternType == Redirect.REGEX) {
      return pattern;
    }
    // Convert wildcard to regex pattern
    let converted = "^";
    for (let i = 0; i < pattern.length; i += 1) {
      const ch = pattern.charAt(i);
      if ("()[]{}?.^$\\+".indexOf(ch) != -1) {
        converted += `\\${ch}`;
      } else if (ch == "*") {
        converted += "(.*?)";
      } else {
        converted += ch;
      }
    }
    converted += "$";
    // logs("Converted pattern from wildcards is.. " + converted);
    return converted;
  },

  _init(o) {
    this.description = o.description || "";
    this.exampleUrl = o.exampleUrl || "";
    this.exampleResult = o.exampleResult || "";
    this.error = null;
    this.includePattern = o.includePattern || "";
    this.excludePattern = o.excludePattern || "";
    this.redirectUrl = o.redirectUrl || "";
    this.patternType = o.patternType || Redirect.WILDCARD;
    this.processMatches = "noProcessing";
    this.disabled = false;
    this.appliesTo = ["main_frame"];
  },

  _includeMatch(url) {
    // var finalurl = "";
    if (!this._rxInclude) {
      return null;
    }
    const matches = this._rxInclude.exec(url);
    if (!matches) {
      return null;
    }
    const resultUrl = this.redirectUrl + matches[0];
    this._rxInclude.lastIndex = 0;
    return resultUrl;
  },

  _excludeMatch(hostname) {
    // logs("inside _excludeMatch.. for url -->" + url);
    if (!this._rxExclude) {
      //  logs("returning false");
      return false;
    }
    // fix for https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/3
    // Url might have tracking parts after ? that might be in excludes
    // In that case, it shouldn't be excluded
    // for example, if url has http://example.com/some/page?utm=twitter.com -> though twitter.com is in Excludes list, shouldn't exclude that as it's in tracking part
    //const url2 = UrlHelper.getHostfromUrl(url).hostname;
    let shouldExclude = this._rxExclude.test(hostname);
    this._rxExclude.lastIndex = 0;
    // The below may not happen at all as background.js just returns when hostname is "t.co"
    // but just to be sure endless redirect won't happen, we will retain the below
    if (hostname == "t.co") {
      shouldExclude = true;
    }
    // logs("shouldExclude --> " + shouldExclude + "for url " + url);
    return shouldExclude;
  },
};

export { Redirect };
