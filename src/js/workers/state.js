/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

const state = {
    // Whether to write debug logs to browser console
    loggingEnabled: false,
    isConfigInitialized: false,
    onBeforeNavigateCountsInWorkerInstance: -1,
    onBeforeNavigateCounts: 0,
    averageOnBeforeNavigateProcessingTime: 0,
    isCommonFileExtensionsListToExcludeAvailable: false,
};

export { state };

// TODO This file is copied from Firefox version.
// Currently only loggingEnabled is used in the chrome version.
// TODO Need to refactor background.js and other worker scripts to use this state fields

// HOST_OS: "",
// // Regex of excludePatterns
// excludePatterns: undefined,
// // Redirects partitioned by request type, so we have to run through the minimum number of redirects for each request.
// partitionedRedirects: {},
// // Store temporarirly Excluded domains
// tempExcludes: [],
// // Store temporarily Included domains
// tempIncludes: [],
// // Store common file extensions to exclude (used to exclude download URLs)
// commonExtensions: [],
// isLoadAllLinksEnabled: false,
// // Cache of urls that have just been redirected to. They will not be redirected again, to
// // stop recursive redirects, and endless redirect chains.
// // Key is url, value is timestamp of redirect.
// ignoreNextRequest: {},
// // Store recently redirected urls to avoid going into loop : Stored as key : url => value: { timestamp:ms, count:1...n};
// justRedirected: {},
// // Used along with justRedirected in logic
// redirectThreshold: 3,
// // filters to clean URLs before loading from wayback machine archive
// filters: [],

// Default value for counts
// initialCounts: {
//     archivedPageLoadsCount: 0,
//     waybackSavescount: 0,
// },
// justreloaded: undefined, // unused in background.js in mv3. TODO
// // Store if readerable pages can load into reader mode directly after redirecting to Wayback Machine snapshot
// isReaderModeEnabled: false,
// // Seed to be used for murmurhash3. This will be updated to an integer that is unique for every session.
// fastHashSeed: 0,
// // Whether to show Stats in popup all the time vs showing upon clicking stats button
// alwaysShowStatsInPopup: false
