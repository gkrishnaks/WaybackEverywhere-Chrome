/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

import { updates } from "../appConfig/updates.js";
import { log } from "../devTools/log.js";

function openUpdatehtml() {
  const url = chrome.runtime.getURL("update.html");
  log("Wayback Everywhere addon installed or updated..");
  chrome.tabs.create({
    url,
  });
}

function dedupExcludes(excludePattern) {
  if (excludePattern.length > 0) {
    let excludePatternArray = excludePattern.split("*|*");
    const oldlength = excludePatternArray.length;
    excludePatternArray[0] = excludePatternArray[0].replace("*", "");
    excludePatternArray[excludePatternArray.length - 1] = excludePatternArray[
      excludePatternArray.length - 1
    ].replace("*", "");
    excludePatternArray = Array.from(new Set(excludePatternArray));
    let shouldSave = false;
    if (excludePatternArray.length !== oldlength) {
      shouldSave = true;
    }
    // additional precaution just in case.. :
    if (excludePatternArray.indexOf("web.archive.org") === -1) {
      excludePatternArray.push("web.archive.org");
    }
    const newPattern = `*${excludePatternArray.join("*|*")}*`;
    const returnObj = { excludePattern: newPattern, shouldSave };
    return returnObj;
  }
  return { excludePattern, shouldSave: false };
}

// eslint-disable-next-line no-extend-native, func-names
String.prototype.replaceAll = function (searchStr, replaceStr) {
  const str = this;

  // escape regexp special characters in search string
  // eslint-disable-next-line no-param-reassign, no-useless-escape
  searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");

  return str.replace(new RegExp(searchStr, "gi"), replaceStr);
};

function handleUpdate(istemporary) {
  // const type = 'update';
  //    let updateJson = "settings/updates.json";
  //  let absUrl = chrome.runtime.getURL(updateJson);
  // updateWorker.postMessage([absUrl, "json", type]);
  // updateWorker.onmessage = function(e) {
  let { changeInAddList } = updates;
  const { changeInRemoveList } = updates;
  const { addToDefaultExcludes } = updates;
  const { removeFromDefaultExcludes } = updates;
  const { showUpdatehtml } = updates;
  const { changeInAddtoFiltersList } = updates;
  const { changeInRemovefromFiltersList } = updates;
  const { addtoFiltersList } = updates;
  const { removefromFiltersList } = updates;
  const { changeinAddtoCommonFileExtensions } = updates;
  const { addtoCommonFileExtensions } = updates;
  const { changeinRemovefromCommonFileExtensions } = updates;
  const { removefromCommonFileExtensions, redirectUrl } = updates;

  // show update html if needed but not durin web-ext runs
  if (showUpdatehtml && istemporary !== true) {
    openUpdatehtml();
  }
  chrome.storage.local.set({ justSaved: [] }); //this was moved to session storage, so set to blank

  // updateWorker.terminate();
  // Add or remove from Excludes
  chrome.storage.local.get(
    {
      redirects: [],
      filters: [],
      commonExtensions: [],
    },
    (response) => {
      // eslint-disable-next-line no-console
      console.log("WaybackEverywhere : handleUpdate-  updating settings");
      const { redirects } = response;
      const filterlist = response.filters;
      const currentCommonExtArray = response.commonExtensions;
      const isRedirectURLChanged = redirects[0].redirectUrl !== redirectUrl;
      if (isRedirectURLChanged) {
        redirects[0].redirectUrl = redirectUrl;
      }
      // Determine changes first..
      // check if we need to add to Excludes list.
      if (
        changeInAddList &&
        addToDefaultExcludes !== undefined &&
        addToDefaultExcludes.length > 0
      ) {
        // if(redirects[0].excludePattern.indexOf(addToDefaultExcludes) === -1 ){
        redirects[0].excludePattern += addToDefaultExcludes;
        log(`the new excludes list is...${redirects[0].excludePattern}`);
      }

      // check if we need to remove from Excludes list.
      if (
        changeInRemoveList &&
        removeFromDefaultExcludes !== undefined &&
        removeFromDefaultExcludes.length > 0
      ) {
        for (let i = 0; i < removeFromDefaultExcludes.length; i += 1) {
          if (removeFromDefaultExcludes[i].indexOf("web.archive.org") === -1) {
            const pattern = `|*${removeFromDefaultExcludes[i]}*`;
            // log("removing this from excl  udest list" + pattern);
            redirects[0].excludePattern =
              redirects[0].excludePattern.replaceAll(pattern, "");
          }
        }
        log(`the new excludes list is. .${redirects[0].excludePattern}`);
      }

      // remove duplicates from Exclude Pattern during every addon update.
      // https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome/issues/59
      const dedupExcludesObj = dedupExcludes(redirects[0].excludePattern);
      if (dedupExcludesObj.shouldSave) {
        changeInAddList = true;
        redirects[0].excludePattern = dedupExcludesObj.excludePattern;
        // eslint-disable-next-line no-console
        console.log("WaybackEverywhere: deduplicated excludes list");
      }

      // check if we need to add to Filters list.
      if (
        changeInAddtoFiltersList &&
        addtoFiltersList !== undefined &&
        addtoFiltersList.length > 0
      ) {
        for (let i = 0; i < addtoFiltersList.length; i += 1) {
          if (filterlist.indexOf(addtoFiltersList[i]) < 0) {
            filterlist.push(addtoFiltersList[i]);
          }
        }
      }

      // check if we need to remove from Filters list.
      if (
        changeInRemovefromFiltersList &&
        removefromFiltersList !== undefined &&
        removefromFiltersList.length > 0
      ) {
        let index = -1;
        for (let i = 0; i < removefromFiltersList.length; i += 1) {
          index = filterlist.indexOf(removefromFiltersList[i]);
          if (index > -1) {
            filterlist.splice(index, 1);
          }
        }
      }

      // Check if we need to add to Common File extensions to exclude.
      if (
        changeinAddtoCommonFileExtensions &&
        addtoCommonFileExtensions !== undefined &&
        addtoCommonFileExtensions.length > 0
      ) {
        // eslint-disable-next-line no-restricted-syntax
        for (const ext of addtoCommonFileExtensions) {
          if (currentCommonExtArray.indexOf(ext) === -1) {
            currentCommonExtArray.push(ext);
          }
        }
      }

      // Check if we need to add to Common File extensions to exclude.
      if (
        changeinRemovefromCommonFileExtensions &&
        removefromCommonFileExtensions !== undefined &&
        removefromCommonFileExtensions.length > 0
      ) {
        // eslint-disable-next-line no-restricted-syntax
        for (const ext1 of removefromCommonFileExtensions) {
          const index = currentCommonExtArray.indexOf(ext1);
          if (index > -1) {
            currentCommonExtArray.splice(index, 1);
          }
        }
      }

      // Save all the changes
      // Save changes in Common Extensions
      if (
        changeinAddtoCommonFileExtensions ||
        changeinRemovefromCommonFileExtensions
      ) {
        // commonExtensions = currentCommonExtArray;
        chrome.storage.local.set(
          {
            commonExtensions: currentCommonExtArray,
          },
          () => {
            log(
              `CommonExtensions saved as .. ${JSON.stringify(
                currentCommonExtArray,
              )}`,
            );
          },
        );
      }

      // Save changes in Filters list
      if (changeInAddtoFiltersList || changeInRemovefromFiltersList) {
        chrome.storage.local.set(
          {
            filters: filterlist,
          },
          () => {
            log(`filters saved as .. ${JSON.stringify(filterlist)}`);
          },
        );
      }

      // Save changes in Exclude pattern
      if (changeInAddList || changeInRemoveList || isRedirectURLChanged) {
        chrome.storage.local.set({
          redirects,
        });
      }
    },
  );
}

export { handleUpdate };
