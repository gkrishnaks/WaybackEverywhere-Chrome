/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

import { onInstalled } from "./onExtensionInstalled.js";
import {
    handleStartup,
    monitorChanges,
    onBeforeNavigate,
    setupRulesIfNeeded,
} from "./background.js";
import { MessageHandler } from "../messageHandlers/messageHandler.js";

// This is the main serviceWorker script that runs in background. This will be setting up the required listeners.

// This runs when extension is first installed, or when it's updated. Initial rule setup happens here.
chrome.runtime.onInstalled.addListener(onInstalled);

// This runs once when th browser profile containing this extension starts up.
chrome.runtime.onStartup.addListener(handleStartup);

// This is for monitoring all changes made to extension's local storage and react to it.
chrome.storage.onChanged.addListener(monitorChanges);

// This takes care of message passing between different parts of the extension for e.g. from popup to worker.
chrome.runtime.onMessage.addListener(MessageHandler);

// This setups the rules by reading it from local storage whenever a tab is activated
chrome.tabs.onActivated.addListener(setupRulesIfNeeded);

/*
  This sets up the listener for looking at browser address bar URL changes and respond to it to see if a domain is excluded or no. 
  If excluded, load live page. If included, then redirect to archived page.

  Implementation notes : 
  If we use webNavigation.onBeforeNavigate with 'onBeforeNavigate' handler, it doesn't seem to invoke when there is a url redirect. For example for URL shorteners.
  Replaced the onBeforeNavigate with onBeforeRequest.

Note - to capture both page load initial and any server redirect, it appears that one has to use either :
1) just onBeforeRequest alone, OR
2) a combination of (onBeforenavigate and onBeforeRedirect), OR
3) a combination of (onBeforeNavigate and onCommitted)
Points 2 and 3 will need more code logic to handle any sequential triggers for the same request. Hence switching to point 1 for now. */

//this is point 1 :
chrome.webRequest.onBeforeRequest.addListener(onBeforeNavigate, {
    urls: ["https://*/*", "http://*/*"],
    types: ["main_frame"],
});

// commented the 2nd and 3rd approach below for reference and in case if it's needed in future :

// Below is the point 2 approach  - onBeforeNavigate + onBeforeRedirect
// chrome.webNavigation.onBeforeNavigate.addListener(onBeforeNavigate, {
//     url: [{ schemes: ["https", "http"] }],
// });

// chrome.webRequest.onBeforeRedirect.addListener(
//     (details) => {
//         if (
//             !details ||
//             details.frameId !== 0 ||
//             !details.redirectUrl ||
//             details?.redirectUrl?.includes("web.archive.org")
//         ) {
//             return;
//         }
//         details.url = details.redirectUrl;
//         onBeforeNavigate(details);
//     },
//     {
//         urls: ["https://*/*", "http://*/*"],
//         types: ["main_frame"],
//     },
// );

// this is point 3  - onBeforeNavigate + onCommitted
// chrome.webNavigation.onBeforeNavigate.addListener(onBeforeNavigate, {
//     url: [{ schemes: ["https", "http"] }],
// });

// chrome.webNavigation.onCommitted.addListener(
//     (details) => {
//         if (
//             !details ||
//             details.frameId !== 0 ||
//             !details.url ||
//             !details.transitionQualifiers?.includes("server_redirect") ||
//             details?.url?.includes("web.archive.org")
//         ) {
//             return;
//         }
//         //onBeforeNavigate(details);
//     },
//     {
//         url: [{ schemes: ["https", "http"] }],
//     },
// );

//TODO replace this isconfigInitialized with tabonactivated or similar from firefox version

// chrome.storage.session.set({
//     isConfigInitialized: true,
// });
