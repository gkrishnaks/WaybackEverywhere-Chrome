/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/
import { Redirect } from "../commonUtils/redirect.js";
import { UrlHelper } from "../commonUtils/parseUrl.js";
import { clearAllTemps, addSitetoExclude } from "../messageHandlers/index.js";
import { state } from "./state.js";

let tempIncludes = [];
let commonExtensions = {};
const counts = {
  archivedPageLoadsCount: 0,
  waybackSavescount: 0,
};
// these unused variables are from mv2 extension, clean up after mv3 version first release
// eslint-disable-next-line no-unused-vars
let tempExcludes = [];
// eslint-disable-next-line no-unused-vars
let isLoadAllLinksEnabled = false;

let disabled = false;

function log(msg) {
  setTimeout(function () {
    if (log.enabled) {
      // eslint-disable-next-line no-console
      console.log(`WaybackEverywhere: ${msg} at ${new Date()}`);
    }
  }, 0);
}

log.enabled = false;

function updateLogging() {
  chrome.storage.local.get(
    {
      logging: false,
    },
    (obj) => {
      log.enabled = obj.logging;
      state.loggingEnabled = obj.logging;
      log(`logging for Wayback Everywhere toggled to..${log.enabled}`);
    },
  );
}

updateLogging();
// Redirects partitioned by request type, so we have to run through
// the minimum number of redirects for each request.
let partitionedRedirects = {};

// Cache of urls that have just been redirected to. They will not be redirected again, to
// stop recursive redirects, and endless redirect chains.
// Key is url, value is timestamp of redirect.
const ignoreNextRequest = {};

// url => { timestamp:ms, count:1...n};
const justRedirected = {};
const redirectThreshold = 3;

let filters = [];

function storeCountstoStorage(requestLog) {
  requestLog(`Setting counts to storage as ${JSON.stringify(counts)}`);
  chrome.storage.local.set({
    counts,
  });
}
// not possible with mv3, mv2 code had these two setIntervals
// setInterval(storeCountstoStorage, 240000);
// 4 minutes once, write counts to disk
// setInterval(clearJustSaved, 240000);
// 4 Minutes once, clear JustSaved based on time.
//justSaved is now  set in session storage

let justSaved = {};
const justreloaded = [];

function clearJustSaved() {
  const keys = Object.keys(justSaved);
  let isChanged = false;
  for (const key of keys) {
    if (Date.now() - justSaved[key] >= 240000) {
      delete justSaved[key];
      isChanged = true;
    }
  }
  if (isChanged) {
    chrome.storage.local.set({ justSaved });
  }
  for (let m = 0; m < justreloaded.length; m += 1) {
    if (Date.now() - Number(justreloaded[m].split("==WBE==")[1]) >= 240000) {
      justreloaded.splice(m, 1);
      // Do not 'break' here, just clear out all old links in justreloaded.
    }
  }
}

// since "t.co" shoterner matches with sites that have "..t.com" in the url as we use RegExp
// As t.co is the most common for links clicked from tweets -
// let's check and return t.co without further processing
// https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/13

// Made the below a loop now after ft.com matched against sites that have ft.com in the URL..
// https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/22
const ambiguousExcludes = {
  "t.co": 1,
  "g.co": 1,
  "ft.com": 1,
  "cnet.co": 1,
  "vine.co": 1,
  "ted.com": 1,
  "f-st.co": 1,
};

function convertExtensionsArrayToMap(extensions) {
  commonExtensions = {};
  if (Array.isArray(extensions) && extensions.length > 0) {
    state.isCommonFileExtensionsListToExcludeAvailable = true;
    for (let i = extensions.length - 1; i >= 0; i--) {
      commonExtensions[extensions[i]] = true;
    }
  }
}

// This is the actual function that gets called for each page load in a tab url bar and must
// decide whether or not we want to redirect.
function checkRedirects(details, requestLog) {
  if (!partitionedRedirects?.["main_frame"]?.[0]) {
    // by this point excludePatterns should already exist as it will be set up in onBeforeNavigate.
    setupRules();
    return;
  }

  // Once wayback redirect url is loaded, we can just return it except when it's in exclude pattern.
  // this is for issue https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/7
  // When already in archived page, Wayback Machine prefixes web.archive.org/web/2/
  // to all URLs in the page
  // For example, when viewing archived site, there's a github link -
  // and if github is in Excludes list,
  // Using this, we load live page of github since it's in excludes list.
  // we may add a switch to Settings page to disable this behaviour at a later time if needed.

  // Need to use once we make Excludepattern array of hosts instead of regex
  // if(excludePatterns.indexOf(host))

  try {
    // Return save page requests right away
    if (details.url.indexOf("web.archive.org/save") > -1) {
      counts.waybackSavescount += 1;
      return;
    }

    const urlDetails = UrlHelper.getHostfromUrl(details.url, requestLog);

    if (urlDetails.hostname.length === 0) {
      // UrlHelper.getHostfromUrl will return empty hostname
      // if you try to load archived version of Wayback Machine itself.
      return;
    }

    if (details.url.indexOf("web.archive.org/web") > -1) {
      // If user tries to click on login, register, or forgot password page,
      // URL might have the words which we can identify
      // and add the site to temporary excludes.
      // Temporary excludes are cleared upon browser restart.
      if (
        urlDetails.url.indexOf("/login") > -1 ||
        urlDetails.url.indexOf("/signin") > -1 ||
        urlDetails.url.indexOf("login.htm") > -1 ||
        urlDetails.url.indexOf("login.php") > -1 ||
        urlDetails.url.indexOf("signin.php") > -1 ||
        urlDetails.url.indexOf("signin.htm") > -1 ||
        urlDetails.url.indexOf("login.asp") > -1 ||
        urlDetails.url.indexOf("login.asp") > -1 ||
        urlDetails.url.indexOf("/join") > -1 ||
        urlDetails.url.indexOf("signup.html") > -1 ||
        urlDetails.url.indexOf("signup.php") > -1 ||
        urlDetails.url.indexOf("signup.asp") > -1 ||
        urlDetails.url.indexOf("/profile/create") > -1 ||
        urlDetails.url.indexOf("/password_reset") > -1 ||
        urlDetails.url.indexOf("/password") > -1 ||
        urlDetails.url.indexOf("/resetpassword") > -1 ||
        urlDetails.url.indexOf("/forgotpassword") > -1 ||
        urlDetails.url.indexOf("/forgot_password") > -1 ||
        urlDetails.url.indexOf("/recoverpassword") > -1 ||
        urlDetails.url.indexOf("/register") > -1 ||
        urlDetails.url.indexOf("register.html") > -1 ||
        urlDetails.url.indexOf("register.asp") > -1 ||
        urlDetails.url.indexOf("register.php") > -1 ||
        urlDetails.url.indexOf("/myaccount") > -1 ||
        urlDetails.url.indexOf("/sign_in") > -1 ||
        urlDetails.url.indexOf("/sign_up") > -1 ||
        urlDetails.url.indexOf("/recover_password") > -1 ||
        urlDetails.url.indexOf("/owa") > -1 ||
        urlDetails.url.indexOf("/Outlook") > -1 ||
        urlDetails.url.indexOf("/outlook") > -1 ||
        urlDetails.url.indexOf("/unsubscribe") > -1 ||
        urlDetails.url.indexOf("/Unsubscribe") > -1 ||
        urlDetails.url.indexOf("/mail") > -1 ||
        urlDetails.url.indexOf("/webmail") > -1 ||
        urlDetails.url.indexOf("/myaccount") > -1 ||
        urlDetails.url.indexOf("/MyAccount") > -1 ||
        urlDetails.url.indexOf("/account") > -1 ||
        urlDetails.url.indexOf("/Account") > -1 ||
        urlDetails.url.indexOf("download.php") > -1 ||
        urlDetails.url.indexOf("downloads.php") > -1 ||
        urlDetails.url.indexOf("/download") > -1
      ) {
        requestLog(
          `Login path found, signup, or other excluded paths found in URL. Adding site to excludes ${
            urlDetails.hostname
          } when you tried to load${details.url}`,
        );
        const request = {};
        request.subtype = "fromContent";
        request.category = "excludethisSite";
        const sender = { tab: {} };
        sender.tab = {};
        sender.tab.id = details.tabId;
        sender.tab.url = details.url;
        // eslint-disable-next-line no-unused-vars
        const sendResponse = (_obj) => {};
        addSitetoExclude(request, sender, sendResponse);
        return;
      }
    }

    if (ambiguousExcludes[urlDetails.hostname]) {
      // We need this if condition to avoid infinite redirects of t.co url into itself.
      // That is, if web.archive.org is prefixed to t.co, just load t.co live url
      // so that this shortener can expand to actual URL
      // If web.archive.org is NOT prefixed, just return -
      // as it can continue to expand to live URL which will get redirected to WM later.
      if (
        details.url.replace("#close", "") !==
        urlDetails.url.replace("#close", "")
      ) {
        requestLog(`Ambigious domain name, loading live : ${urlDetails.url}}`);
        chrome.tabs.update(details.tabId, {
          url: urlDetails.url,
        });

        return;
      }
      requestLog(
        `Ambigious domain name, loading live : ${details.url} in tab : ${details.tabId}`,
      );
      return;
    }

    // https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/20
    // Issue happens when a blog redirects to medium globalidentify for some reason
    // .. as we don't have medium in Excludes list -
    // since medium.com articles work fine with Wayback machine
    // .. just programatically add these redirect blogs to excludes list and load live page.
    // do this programatically instead of adding to settings.json,
    // so that user can keep building her Excludes list using this too.
    // Example :
    // https://medium.com/m/global-identity?redirectUrl=https://blog.mapbox.com/hd-vector-maps-open-standard-335a49a45210

    if (
      urlDetails.hostname === "medium.com" &&
      urlDetails.url.indexOf("global-identity?redirect") > -1 &&
      details.url.indexOf("web.archive.org") > -1
    ) {
      const request = {};
      request.subtype = "fromContent";
      request.category = "addtoExclude";
      const sender = { tab: {} };
      sender.tab = {};
      sender.tab.id = details.tabId;
      const index = urlDetails.url.indexOf("redirectUrl=") + 12;
      sender.tab.url = `https://web.archive.org/web/2/${decodeURIComponent(
        urlDetails.url.substring(index),
      )}`;
      const sendResponse = () => {};
      addSitetoExclude(request, sender, sendResponse);
      requestLog(
        `Adding this site to exclude as this is a medium redirect that does not work with Wayback Machine ${details.url} in tab ${details.tabId}`,
      );
      return;
    }

    if (details.url.indexOf("web.archive.org/web") > -1) {
      if (state.isCommonFileExtensionsListToExcludeAvailable) {
        // #23 - https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/23
        // Load live url when url ends with common file extensions
        // so that user can download a file easily example.com/path/to/dir/file.zip
        // let isDownloadlink = false;

        let liveURL = urlDetails.url.toLowerCase();
        if (liveURL.endsWith("#close")) {
          // eslint-disable-next-line prefer-destructuring
          liveURL = liveURL.split("#close")[0];
        }
        let index = liveURL.indexOf("?");
        if (index > -1) {
          liveURL = liveURL.substring(0, index);
          // index = -1;
        }
        if (liveURL.endsWith("/")) {
          liveURL = liveURL.substring(0, liveURL.length - 1);
        }
        index = liveURL.indexOf("#");
        if (index > -1) {
          liveURL = liveURL.substring(0, index);
        }
        index = liveURL.lastIndexOf(".");
        const isDownloadlink = commonExtensions?.[liveURL.substring(index)];

        if (isDownloadlink) {
          requestLog(
            `File download link detected based on file extension, load the live link instead as Wayback Machine may not cache files. ${urlDetails?.url}`,
          );
          chrome.tabs.update(details.tabId, {
            url: urlDetails.url,
          });

          return;
        }
      }

      // Issue 12   https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/12
      const toSaveurl = urlDetails.url.replace("#close", "");
      if (justSaved[toSaveurl]) {
        requestLog(
          `this page is in justSaved, so loading archived version ${toSaveurl}`,
        );
        return;
      }

      const resultForLiveURL = partitionedRedirects?.[
        "main_frame"
      ]?.[0].getMatch(urlDetails.url, urlDetails.hostname);
      if (tempIncludes.length === 0) {
        if (resultForLiveURL.isExcludeMatch) {
          requestLog(
            `Loading live since this page is excluded : ${urlDetails.url}`,
          );
          chrome.tabs.update(details.tabId, {
            url: urlDetails.url,
          });
          return; // mv2: { redirectUrl: urlDetails.url };
        }
        return;
      }
      if (tempIncludes.indexOf(urlDetails.hostname) > -1) {
        return;
      }
      if (resultForLiveURL.isExcludeMatch) {
        requestLog(
          `Loading live since this page is excluded : ${urlDetails.url}`,
        );
        chrome.tabs.update(details.tabId, {
          url: urlDetails.url,
        });
        return;
      }

      return;
    }

    const timestamp = ignoreNextRequest[details.url];
    if (timestamp) {
      requestLog(
        ` Ignoring ${details.url}, was just redirected ${
          new Date().getTime() - timestamp
        }ms ago`,
      );
      delete ignoreNextRequest[details.url];
      return;
    }

    // const r = partitionedRedirects?.["main_frame"]?.[0];
    // if user clicks on search result link :
    if (
      details.url.startsWith("https://www.google.com/url?q=") ||
      details.url.startsWith("http://www.google.com/url?q=")
    ) {
      const split = details.url.split("google.com/url?q=");
      if (split.length > 1) {
        // eslint-disable-next-line prefer-destructuring
        details.url = split[1];
      }
    }
    const result = partitionedRedirects?.["main_frame"]?.[0].getMatch(
      details.url,
      urlDetails.hostname,
    );
    /* wmAvailabilityCheck( details.url,function onSuccess(wayback_url,url){
        log('wayback wmAvailabilityCheck passed ->  wayback_url = ' + wayback_url + ' url ' + url)
      },function onfail(){log(' wayback wmAvailabilityCheck failed')}); */

    if (result.isMatch) {
      // Check if we're stuck in a loop where we keep redirecting this, in that
      // case ignore!
      requestLog(" checking if we have just redirected to avoid loop");
      const data = justRedirected[details.url];

      const threshold = 3000;
      if (!data || new Date().getTime() - data.timestamp > threshold) {
        // Obsolete after 3 seconds
        justRedirected[details.url] = {
          timestamp: new Date().getTime(),
          count: 1,
        };
      } else {
        data.count += 1;
        justRedirected[details.url] = data;
        if (data.count >= redirectThreshold) {
          requestLog(
            ` Ignoring ${details.url} because we have redirected it ${
              data.count
            } times in the last ${threshold}ms`,
          );
          return;
        }
      }

      ignoreNextRequest[result.redirectTo] = new Date().getTime();

      counts.archivedPageLoadsCount += 1;
      // console.log(JSON.stringify(ignoreNextRequest))
      // console.log(counts.archivedPageLoadsCount);

      // requestLog(`counts updated to ${counts.archivedPageLoadsCount}`);
      // Issue 10 - https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/10
      // Remove ?utm and others and redirect only direct clean URL to wayback machine
      result.redirectTo = UrlHelper.cleanUrlsOnFilters(
        result.redirectTo,
        filters,
        requestLog,
      );
      // requestLog(`Redirecting to ......${result.redirectTo}`);
      // eslint-disable-next-line no-await-in-loop
      chrome.tabs.update(details.tabId, {
        url: result.redirectTo,
      });
      requestLog(
        ` Redirecting ${details.url} ===> ${result.redirectTo}, type: ${
          details.type
        }, and archivedPageLoadsCount updated to ${counts.archivedPageLoadsCount}`,
      );
      return true;
      // return {
      //   redirectUrl: result.redirectTo
      // }; //mv2
    }
    requestLog(
      `No rules apply, no action taken: url: ${details.url} tabId: ${details.tabId}`,
    );
  } catch (e) {
    requestLog(`Error ${e}`);
  }
}

// Monitor changes in data, and setup everything again.
// This could probably be optimized to not do everything on every change
// but why bother
// eslint-disable-next-line no-unused-vars
function monitorChanges(changes, namespace) {
  //handle storage.session changes first
  if (namespace === "session") {
    if (changes.disabled) {
      disabled = changes.disabled.newValue;
    }
    if (changes.justSaved) {
      justSaved = changes.justSaved.newValue;
    }
    return;
  }

  //below are storage.local
  if (changes.redirects) {
    createPartitionedRedirects(changes.redirects.newValue);
  }

  if (changes.logging) {
    updateLogging();
  }
  if (changes.tempIncludes) {
    log(`tempIncludes changed to${changes.tempIncludes.newValue}`);
    tempIncludes = changes.tempIncludes.newValue;
  }
  if (changes.tempExcludes) {
    log(`tempExcludes changed to${changes.tempExcludes.newValue}`);
    tempExcludes = changes.tempExcludes.newValue;
  }

  if (changes.filters) {
    filters = changes.filters.newValue;
  }

  if (changes.isLoadAllLinksEnabled) {
    isLoadAllLinksEnabled = changes.isLoadAllLinksEnabled.newValue;
  }

  if (changes.counts) {
    // This is needed only when user resets stats to 0 from advanceduser page
    if (
      changes.counts.newValue.archivedPageLoadsCount === 0 &&
      changes.counts.newValue.waybackSavescount === 0
    ) {
      counts.archivedPageLoadsCount = 0;
      counts.waybackSavescount = 0;
    }
  }
  if (changes.commonExtensions) {
    commonExtensions = convertExtensionsArrayToMap(
      changes.commonExtensions.newValue,
    );
  }
}

function testPartitionedRedirects() {
  try {
    // eslint-disable-next-line no-unused-vars
    const result = partitionedRedirects?.["main_frame"]?.[0]?.getMatch(
      "https://example.com",
      "example.com",
    );
    partitionedRedirects?.["main_frame"]?.[0]?.getMatch(
      "http://example.com",
      "example.com",
    );
    // log(
    //   "WaybackEverywhere : Test Partitioned Redirects for example.com : result : " +
    //     result.isMatch,
    // );
  } catch (e) {
    log(
      "WaybackEverywhere : Will not redirect. Error in createPartitionedRedirects ",
      e,
    );
  }
}
// TODO: move Remove from Excludes from popup.js to here
// i.e Temporary incldue or Include should go here, currently it's in popup.js

function createPartitionedRedirects(redirects, fromOnBeforeNavigate = false) {
  partitionedRedirects = {};
  try {
    if (redirects?.[0]) {
      const redirect = new Redirect(redirects[0]);
      redirect.compile();

      const requestType = redirect.appliesTo[0];

      partitionedRedirects[requestType] = [redirect];

      if (fromOnBeforeNavigate) {
        state.onBeforeNavigateCountsInWorkerInstance = -1;
        return;
      }
      testPartitionedRedirects();
    }
  } catch (e) {
    log(
      "WaybackEverywhere : Will not redirect. Error in createPartitionedRedirects ",
      e,
    );
  }
}

async function setupRulesIfNeeded() {
  if (
    !partitionedRedirects?.["main_frame"]?.[0] ||
    !state.isConfigInitialized
  ) {
    await setupRules();
    return;
  }
  testPartitionedRedirects();
}

async function setupRules(requestLog = log, fromOnBeforeNavigate = false) {
  requestLog(" exclude patterns not available, initializing from storage");

  const obj = await chrome.storage.local.get({
    redirects: [],
    commonExtensions: [],
    filters: [],
    tempExcludes: [],
    tempIncludes: [],
    counts: {},
  });

  counts.archivedPageLoadsCount = obj.counts?.archivedPageLoadsCount ?? 0;
  counts.waybackSavescount = obj.counts?.waybackSavescount ?? 0;
  tempExcludes = obj.tempExcludes;
  tempIncludes = obj.tempIncludes;
  convertExtensionsArrayToMap(obj.commonExtensions);
  filters = obj.filters;
  const { redirects } = obj;
  if (
    redirects?.length === 0 ||
    !redirects?.[0]?.excludePattern ||
    redirects?.[0]?.excludePattern?.length === 0
  ) {
    requestLog(" No redirects defined");
    return;
  }
  // (we need to make ExcludePattern an array of hosts, currently it's regex)

  createPartitionedRedirects(redirects, fromOnBeforeNavigate);

  const result = await chrome.storage.session.get({
    onBeforeNavigateCounts: 0,
    disabled: false,
    averageOnBeforeNavigateProcessingTime: 0,
    justSaved: {},
  });
  justSaved = result.justSaved;
  disabled = result.disabled;
  state.onBeforeNavigateCounts = result.onBeforeNavigateCounts;
  state.averageOnBeforeNavigateProcessingTime =
    result.averageOnBeforeNavigateProcessingTime;
  state.isConfigInitialized = true;
  //chrome.storage.session.set({ isConfigInitialized: true });
}

// Sets up the listener, partitions the redirects, creates the appropriate filters etc.
async function onBeforeNavigate(details) {
  function requestLog(message) {
    log(`ReqID: ${details.requestId} : ${message}`);
  }

  const processingStartMs = performance.now();

  if (
    !state.isConfigInitialized ||
    !partitionedRedirects?.["main_frame"]?.[0]
  ) {
    await setupRules(requestLog, true);
  }

  if (
    !details ||
    details.frameId !== 0 ||
    !details.url ||
    !details.tabId ||
    !details.method ||
    !details.type ||
    details.type !== "main_frame" ||
    details.method !== "GET" ||
    details.tabId < 0 // Added after observing a negative tabId show up in Edge Canary Android which would later fail in tabs.update()
  ) {
    return;
  }

  requestLog(
    `onBeforeNavigate  started for main frame URL .. Check if temporarily disabled, otherwise proceed to evaluate the rules  tab ID : ${details.tabId}, url: ${details.url}`,
  );

  if (disabled) {
    requestLog(
      ` Wayback Everywhere is currently disabled or not initialized yet, state.isConfigInitialized:  ${state.isConfigInitialized} , isDisabled : ${disabled}`,
    );
    return;
  }

  const isRedirected = checkRedirects(details, requestLog) ?? false;
  const processingTime = Number(
    (performance.now() - processingStartMs).toFixed(4),
  );
  if (state.onBeforeNavigateCountsInWorkerInstance <= 0) {
    if (state.onBeforeNavigateCountsInWorkerInstance === 0) {
      state.onBeforeNavigateCounts += 1;
    } else {
      testPartitionedRedirects();
    }
    state.onBeforeNavigateCountsInWorkerInstance += 1;

    requestLog(
      ` Finished event handler for ${details.url} in tab: ${details.tabId} in  ${processingTime} ms. Result : ${isRedirected}`,
    );
    return;
  }

  state.averageOnBeforeNavigateProcessingTime =
    (state.averageOnBeforeNavigateProcessingTime *
      (state.onBeforeNavigateCounts - 1) +
      processingTime) /
    state.onBeforeNavigateCounts;

  const oldOnBeforeNavigateCounts = state.onBeforeNavigateCounts;
  state.onBeforeNavigateCounts += 1;

  setTimeout(function () {
    requestLog(
      ` Finished event handler for ${details.url} in tab: ${details.tabId} in  ${processingTime} ms. Result : ${isRedirected}`,
    );

    clearJustSaved();
    storeCountstoStorage(requestLog);
    // start the average running from 2nd request onwards considering as its entry for average

    performanceLog(
      details,
      processingTime,
      requestLog,
      oldOnBeforeNavigateCounts,
      state.averageOnBeforeNavigateProcessingTime,
      isRedirected,
    );
  }, 0);
}

function performanceLog(
  details,
  timeMs,
  requestLog,
  onBeforeNavigateCounts,
  onBeforeNavigateAverage,
  isRedirected,
) {
  const avg = Number(onBeforeNavigateAverage.toFixed(4));

  const object = {
    extension: "WaybackEverywhere",
    event: "onBeforeNavigate",
    time: new Date(),
    requestID: details.requestId,
    url: details.url,
    tab: details.tabId,
    timeTaken: `${timeMs} ms`,
    onBeforeNavigateCounts,
    averageOnBeforeNavigateProcessingTime: `${avg} ms`,
    isRedirected,
  };
  requestLog(
    ` Average onBeforeNavigate processing time ${avg} ms over ${onBeforeNavigateCounts} invocations`,
  );
  chrome.storage.session.set({
    averageOnBeforeNavigateProcessingTime: avg,
    onBeforeNavigateCounts,
  });
  if (log.enabled) {
    console.table(object);
  }
}

// this won't work in mv3. Mv2 specific code, find alternative. TODO

// Added the below to hande a very rare case where Wayback throws "504" error when Saving page.
// Manually reloading the page was enough to get it work next time.
// This will just reload the page once and stop reloading after that if it continues as
// .. it assigned url to justreloaded variable

// Find out if 504 is thrown by the saved page or by WM itself -
// Need to Comment out the below if WM is actually the one that shows this 504

// function reloadPage(tabId, tabUrl) {
//   let shouldReload = false;
//   for (let i = 0; i < justreloaded.length; i++) {
//     if (justreloaded[i].indexOf(tabUrl) < 0) {
//       shouldReload = true;
//       justreloaded.push(tabUrl + "==WBE==" + Date.now());
//       break;
//     }
//   }
//   if (shouldReload) {
//     chrome.tabs.reload(
//       tabId,
//       {
//         bypassCache: true
//       },
//       function () {
//         log("Page reloaded");
//       }
//     );
//   }
// }
// chrome.webRequest.onCompleted.addListener(
//   function(details) {
//     /*if (details.type == "main_frame") {
//     console.log("status code is " + details.statusCode + " in url " + details.url);
//   } */
//     if (
//       details.type == "main_frame" &&
//       (details.statusCode == 504 || details.statusCode == 503)
//     ) {
//       reloadPage(details.tabId, details.url);
//     }
//   },
//   {
//     urls: ["*://web.archive.org/*"]
//   }
// );

// First time setup
// updateIcon();

chrome.storage.session.get(
  {
    disabled: false,
  },
  (obj) => {
    if (!obj.disabled) {
      // onBeforeNavigate();
      log("Wayback Everywhere is enabled");
    } else {
      log(`Wayback Everywhere is disabled...${obj.disabled}`);
    }
  },
);

log(" Wayback Everywhere starting up...");

// chrome.tabs.onUpdated.addListener(tabUpdatedListener);//TODO

function handleStartup() {
  const msg =
    "Handle startup - fetch counts, fetch readermode setting, fetch appdisabled setting, clear out any temp excludes or temp includes";

  console.log(`WaybackEverywhere: ${msg} at ${new Date()}`);

  chrome.storage.local.get(
    {
      isLoadAllLinksEnabled: true,
      operationMode: false,
    },
    async (obj) => {
      isLoadAllLinksEnabled = obj.isLoadAllLinksEnabled;
      // operationMode -> false is default behaviour of turning on WBE when browser loads.
      // true - if user wishes to start browser with WBE disabled
      // eslint-disable-next-line no-console
      console.log(
        `Wayback Everywhere - Operation mode: ${obj.operationMode} If true, opted to start browser with Wayback disabled. Can be temporarily enabled from popup menu`,
      );
      await chrome.storage.session.set({
        disabled: obj.operationMode,
        //  isConfigInitialized: true,
      });
      setupRulesIfNeeded();
    },
  );

  /*
    // Enable on startup - Popup button is "Temporarily disable.."
    // as user can do full disable from addon/extension page anyway
    chrome.storage.local.set({
      disabled: false
    }); */
  // Disable logging on startup
  // disabled in local storage was in mv2 version, make it false as it should be unused now in mv3
  chrome.storage.local.set({
    logging: false,
    disabled: false,
  });
  // eslint-disable-next-line no-unused-vars
  const sendResponse = (_obj) => {};
  clearAllTemps(sendResponse);
}

export { handleStartup, monitorChanges, onBeforeNavigate, setupRulesIfNeeded };
