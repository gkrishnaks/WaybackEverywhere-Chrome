/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2025 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

chrome.runtime.onMessage.addListener((request, _sender, sendResponse) => {
  if (request.type === "getAllFirstPartylinks") {
    const anchorsArray = [];
    const anchors = [].slice.call(document.getElementsByTagName("a")); // TODO Array.from
    for (let i = 0; i < anchors.length; i += 1) {
      if (anchors[i].href) {
        anchorsArray.push(anchors[i].href);
      }
    }
    // console.log(anchorsArray);
    chrome.runtime.sendMessage({
      type: "openAllLinks",
      subtype: "fromPopup",
      data: anchorsArray,
      selector: request.selector,
    });
    sendResponse({
      data: anchorsArray,
    });
  }
  return true;
});
